INSERT INTO widget (id, name, price) VALUES (1001, 'widget 1', 12);
INSERT INTO widget (id, name, price) VALUES (1002, 'widget 2', 10);
INSERT INTO widget (id, name, price) VALUES (1003, 'widget 3', 1);
INSERT INTO widget (id, name, price) VALUES (1004, 'widget 4', 122);
INSERT INTO widget (id, name, price) VALUES (1005, 'widget 5', 34);

INSERT INTO store (id, name, location) VALUES (2001, 'store 1', 'location 1');
INSERT INTO store (id, name, location) VALUES (2002, 'store 2', 'location 2');
INSERT INTO store (id, name, location) VALUES (2003, 'store 3', 'location 3');
INSERT INTO store (id, name, location) VALUES (2004, 'store 4', 'location 4');
INSERT INTO store (id, name, location) VALUES (2005, 'store 5', 'location 5');

INSERT INTO sale (id, store_id, widget_id, units) VALUES (3001,2001 , 1002, 2 );
INSERT INTO sale (id, store_id, widget_id, units) VALUES (3002,2001 , 1003, 5 );
INSERT INTO sale (id, store_id, widget_id, units) VALUES (3003,2005 , 1005, 1 );
INSERT INTO sale (id, store_id, widget_id, units) VALUES (3004,2004 , 1001, 10 );
INSERT INTO sale (id, store_id, widget_id, units) VALUES (3005,2001 , 1003, 3 );



