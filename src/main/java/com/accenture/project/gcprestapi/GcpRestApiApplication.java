package com.accenture.project.gcprestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GcpRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GcpRestApiApplication.class, args);
	}

}
