package com.accenture.project.gcprestapi.service;

import com.accenture.project.gcprestapi.entity.ResponseData;
import com.accenture.project.gcprestapi.entity.Sale;
import com.accenture.project.gcprestapi.entity.SaleData;

import java.util.List;

public interface SaleService {
    List<Sale> fetchSalesForStoreWithId(final int storeId);

    ResponseData recordSaleTransaction(final SaleData saleData);
}
