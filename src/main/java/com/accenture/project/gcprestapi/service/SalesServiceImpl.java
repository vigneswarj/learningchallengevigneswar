package com.accenture.project.gcprestapi.service;

import com.accenture.project.gcprestapi.entity.*;
import com.accenture.project.gcprestapi.repository.SaleRepository;
import com.accenture.project.gcprestapi.repository.StoreRepository;
import com.accenture.project.gcprestapi.repository.WidgetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SalesServiceImpl implements SaleService {

    @Autowired
    StoreRepository storeRepository;

    @Autowired
    WidgetRepository widgetRepository;

    @Autowired
    SaleRepository saleRepository;

    @Override
    public List<Sale> fetchSalesForStoreWithId(final int storeId) {
        return saleRepository.findAllByStoreId(storeId);
    }

    public ResponseData recordSaleTransaction(SaleData saleData) {
        Optional<Store> store = storeRepository.findById(saleData.getStoreId());
        Optional<Widget> widget = widgetRepository.findById(saleData.getWidgetId());
        Sale sale = new Sale();
        if(store.isPresent() && widget.isPresent()) {
            sale.setStore(store.get());
            sale.setWidget(widget.get());
            sale.setUnits(saleData.getUnitSoldCount());
            saleRepository.save(sale);
        }
        return new ResponseData();
    }
}
