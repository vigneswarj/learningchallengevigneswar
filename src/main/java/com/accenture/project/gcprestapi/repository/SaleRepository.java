package com.accenture.project.gcprestapi.repository;

import com.accenture.project.gcprestapi.entity.Sale;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SaleRepository extends JpaRepository<Sale, Integer> {
    List<Sale> findAllByStoreId(int storeId);
}
