package com.accenture.project.gcprestapi.repository;

import com.accenture.project.gcprestapi.entity.Sale;
import com.accenture.project.gcprestapi.entity.Store;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StoreRepository extends JpaRepository<Store, Integer> {
}
