package com.accenture.project.gcprestapi.repository;

import com.accenture.project.gcprestapi.entity.Sale;
import com.accenture.project.gcprestapi.entity.Widget;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WidgetRepository extends JpaRepository<Widget, Integer> {
}
