package com.accenture.project.gcprestapi.controller;

import com.accenture.project.gcprestapi.entity.ResponseData;
import com.accenture.project.gcprestapi.entity.Sale;
import com.accenture.project.gcprestapi.entity.SaleData;
import com.accenture.project.gcprestapi.service.SaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("v1/api/sale")
public class saleController {

    @Autowired
    SaleService saleService;

    @GetMapping("/{storeId}")
    ResponseEntity<List<Sale>> salesByStoreId(@PathVariable final int storeId) {
        return ResponseEntity
                .status(HttpStatus.FOUND.value())
                .body(saleService.fetchSalesForStoreWithId(storeId));
    }

    @PostMapping
    ResponseEntity<ResponseData> recordSales(@RequestBody SaleData saleData) {
        return ResponseEntity
                .status(HttpStatus.FOUND.value())
                .body(saleService.recordSaleTransaction(saleData));
    }


}
