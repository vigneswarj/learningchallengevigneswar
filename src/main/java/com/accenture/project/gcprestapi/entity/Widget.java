package com.accenture.project.gcprestapi.entity;

import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Table(name = "widget")
@Entity
@Data
@NoArgsConstructor
public class Widget {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(unique = true)
    @NotNull
    private String name;

    @NotNull
    private double price;
}
