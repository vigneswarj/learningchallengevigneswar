package com.accenture.project.gcprestapi.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SaleData {
    private int storeId;
    private int widgetId;
    private int unitSoldCount;
}
