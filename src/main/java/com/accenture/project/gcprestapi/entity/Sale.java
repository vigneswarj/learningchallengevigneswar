package com.accenture.project.gcprestapi.entity;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "sale")
@Entity
@Data
@NoArgsConstructor
public class Sale {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="widget_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Widget widget;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="store_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Store store;

    @NotNull
    private int units;

}
